using System;
using UnityEngine;

public class RemoveFallenBlocks : MonoBehaviour
{
	private const float NOT_DEAD_TIME = float.MinValue;
	private float initialRotation;
	private Vector2 previousPosition;
	private float previousRotation;
	private GameObject go;
	private float disposeTimer = NOT_DEAD_TIME;
	private double rotationThreshold = 15;
	
	public int removalDelay = 1;

	// Start is called before the first frame update
	void Start()
	{
		go = gameObject;
		previousPosition = go.transform.position;
		initialRotation = CurrentRotation();
	}

	private void FixedUpdate()
	{
		var currentPosition = go.transform.position;
		var currentRotation = CurrentRotation() - initialRotation;

		if (IsTimeToDispose())
			DisposeBlock();
		else
		if (IsRotating() || IsInMotion())
			ResetDeadTimer();
		else if (IsTooRotated())
			SetDeadTimer();
		else
			ResetDeadTimer();

		previousPosition = currentPosition;
		previousRotation = currentRotation;

		bool IsRotating() =>
			Math.Abs((currentRotation - previousRotation) / Time.deltaTime) > rotationThreshold;

		bool IsInMotion() =>
			Math.Abs(currentPosition.x - previousPosition.x) / Time.deltaTime > 1 ||
			Math.Abs(currentPosition.y - previousPosition.y) / Time.deltaTime > 1;

		bool IsTooRotated() => 
			currentRotation > rotationThreshold;

		bool IsTimeToDispose() =>
			disposeTimer > 0 && Time.time > disposeTimer + removalDelay;
	}

	private void DisposeBlock()
	{
		EventManager.TriggerEvent("BlockDisposed", gameObject);
		Destroy(go);
	}

	private void SetDeadTimer()
	{
		if (disposeTimer < 0)
			disposeTimer = Time.time;
	}

	private void ResetDeadTimer() => 
		disposeTimer = NOT_DEAD_TIME;

	private void SetColor(Color color) =>
		go.GetComponent<SpriteRenderer>().color = color;

	private float CurrentRotation() =>
		DegreesOff90(go.transform.rotation.eulerAngles.z);

	private float DegreesOff90(float rotation)
	{
		var result = rotation % 90;

		return result < 45
			? result
			: 90 - result;
	}
}