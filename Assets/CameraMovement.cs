using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraMovement : MonoBehaviour
{
	private Vector3 target;
	private Transform cameraTransform;
	private GameDirector gameDirector;
	private UnityAction onToggleMusic;
	private AudioSource audioSource;

	void Start()
	{
		cameraTransform = Camera.main.transform;
		target = cameraTransform.position;
		
		audioSource = GetComponent<AudioSource>();
	}

	private void Awake()
	{
		onToggleMusic = ToggleMusic;
		
		gameDirector = FindObjectOfType<GameDirector>();
		gameDirector.onHeightChanged = position =>
		{
			var cameraTransformPosition = cameraTransform.position;
			target = new Vector3(cameraTransformPosition.x, position.y + 2, cameraTransformPosition.z);
		};
	}

	void Update()
	{
		var currentPosition = cameraTransform.position;
		cameraTransform.position = currentPosition + (target - currentPosition) * Time.deltaTime;
	}
	
	private void OnEnable()
	{
		EventManager.StartListening("ToggleMusic", onToggleMusic);
	}

	private void OnDisable()
	{
		EventManager.StopListening("ToggleMusic", onToggleMusic);
	}

	private void ToggleMusic()
	{
		if (audioSource.isPlaying)
		{
			audioSource.Stop();
		}
		else
		{
			audioSource.Play();
		}
	}
}