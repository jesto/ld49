using UnityEngine;
using UnityEngine.Events;

public class Block : MonoBehaviour
{
    private bool isFalling;
    private uint movementDivider;

    // Start is called before the first frame update
    void Start()
    {
        var blockRigidbody = GetComponent<Rigidbody2D>();
        blockRigidbody.isKinematic = true;
        isFalling = true;
    }

    public void SetMovementDivider(uint movementDivider)
    {
        this.movementDivider = movementDivider;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("KillZone"))
        {
            EventManager.TriggerEvent("BlockDisposed", gameObject);
            Destroy(gameObject);
            return;
        }
        
        if (isFalling)
        {
            isFalling = false;
            MoveBackBeforeApplyingPhysics();

            GetComponent<Rigidbody2D>().isKinematic = false;
            GetComponent<Collider2D>().isTrigger = false;

            EventManager.TriggerEvent("ActiveBlockStoppedFalling");
            
            StopListeningToMovementEvents();
            
            void MoveBackBeforeApplyingPhysics() => transform.position = latestPosition;
        }
    }

    private UnityAction onMoveLeft;
    private UnityAction onMoveRight;
    private UnityAction onRotate;
    private UnityAction onDropDown;
    private Vector3 latestPosition;

    private void Awake()
    {
        onMoveLeft = OnMoveLeft;
        onMoveRight = OnMoveRight;
        onRotate = OnRotate;
        onDropDown = OnDropDown;
    }

    private void OnEnable()
    {
        EventManager.StartListening("MoveLeft", onMoveLeft);
        EventManager.StartListening("MoveRight", onMoveRight);
        EventManager.StartListening("Rotate", onRotate);
        EventManager.StartListening("DropDown", onDropDown);
    }

    private void OnDisable() => StopListeningToMovementEvents();

    private void StopListeningToMovementEvents()
    {
        EventManager.StopListening("MoveLeft", onMoveLeft);
        EventManager.StopListening("MoveRight", onMoveRight);
        EventManager.StopListening("Rotate", onRotate);
        EventManager.StopListening("DropDown", onDropDown);
    }

    private void OnMoveLeft()
    {
        if (transform.position.x < -2.5)
            return;
        
        transform.position += Vector3.left/movementDivider;
    }    

    private void OnMoveRight()
    {
        if (transform.position.x > 2.5)
            return;
        
        transform.position += Vector3.right/movementDivider;
    }

    private void OnRotate()
    {
        transform.Rotate(Vector3.forward, 90, Space.World);
    }
    
    private void OnDropDown()
    {
        if (transform.position.y < 0)
            return;

        latestPosition = transform.position;
        transform.position += new Vector3(0, -1f , 0);
    }

    public void MoveDown(float blockFallSpeed)
    {
        if (isFalling == false)
            return;

        latestPosition = transform.position;
        transform.position += new Vector3(0, -0.01f * blockFallSpeed, 0);
    }
}
