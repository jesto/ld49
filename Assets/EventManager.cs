using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class EventManager : MonoBehaviour
{
	private Dictionary<string, UnityEventBase> eventDictionary;

	private static EventManager eventManager;

	public static EventManager instance
	{
		get
		{
			if (!eventManager)
			{
				eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

				if (!eventManager)
				{
					Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
				}
				else
				{
					eventManager.Init();
				}
			}

			return eventManager;
		}
	}

	void Init()
	{
		eventDictionary ??= new Dictionary<string, UnityEventBase>();
	}

	public static void StartListening(string eventName, UnityAction listener)
	{
		if (instance.eventDictionary.TryGetValue(eventName, out var thisEvent))
		{
			(thisEvent as UnityEvent)?.AddListener(listener);
		}
		else
		{
			var ue = new UnityEvent();
			ue.AddListener(listener);
			instance.eventDictionary.Add(eventName, ue);
		}
	}

	public static void StartListening<T>(string eventName, UnityAction<T> listener)
	{
		if (instance.eventDictionary.TryGetValue(eventName, out var thisEvent))
		{
			((UnityEvent<T>)thisEvent)?.AddListener(listener);
		}
		else
		{
			var ue = new UnityEvent<T>();
			ue.AddListener(listener);
			instance.eventDictionary.Add(eventName, ue);
		}
	}

	public static void StopListening(string eventName, UnityAction listener)
	{
		if (eventManager == null) return;
		if (instance.eventDictionary.TryGetValue(eventName, out var thisEvent))
		{
			((UnityEvent)thisEvent)?.RemoveListener(listener);
		}
	}

	public static void StopListening<T>(string eventName, UnityAction<T> listener)
	{
		if (eventManager == null) return;
		if (instance.eventDictionary.TryGetValue(eventName, out var thisEvent))
		{
			((UnityEvent<T>)thisEvent)?.RemoveListener(listener);
		}
	}

	public static void TriggerEvent(string eventName)
	{
		if (instance.eventDictionary.TryGetValue(eventName, out var thisEvent))
		{
			((UnityEvent)thisEvent)?.Invoke();
		}
	}

	public static void TriggerEvent<T>(string eventName, T data)
	{
		if (instance.eventDictionary.TryGetValue(eventName, out var thisEvent))
		{
			((UnityEvent<T>)thisEvent).Invoke(data);
		}
	}
}