using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ScoringManager : MonoBehaviour
{
    [FormerlySerializedAs("Current ScoreText")] 
    public GameObject currentScoreTextGameObject;
    
    [FormerlySerializedAs("Highest ScoreText")] 
    public GameObject highestScoreTextGameObject;

    private Text currentScoreTextComponent;
    private Text highestScoreTextComponent;
    private int playerScore = 0;
    private int highestScore = 0;

    void Start()
    {
        currentScoreTextComponent = currentScoreTextGameObject.GetComponent<Text>();
        highestScoreTextComponent = highestScoreTextGameObject.GetComponent<Text>();
    }

    void Update()
    {
        currentScoreTextComponent.text = $"Current score: {playerScore}";
        highestScoreTextComponent.text = $"Highest score: {highestScore}";
    }

    public void OnBlockSpawned()
    {
        playerScore++;

        if (playerScore > highestScore)
            highestScore = playerScore;
    }

    public void OnBlockDisposed()
    {
        playerScore--;
    }
}
