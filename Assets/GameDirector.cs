using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using Random = System.Random;

public class GameDirector : MonoBehaviour
{
    public GameObject[] blockPrefabs;
    public uint minimumTimeBetweenBlockSpawns;
    public GameObject blockSpawnPosition;
    public float blockFallSpeed;
    public float spawnHightBuffer;
    public uint blockMovementDivider;

    private double nextSpawn;
    private Random random;
    private GameObject activeBlock;
    private ScoringManager scoringManager;
    private Block activeBlockScript;

    // Start is called before the first frame update
    void Start()
    {
        random = new Random();
        scoringManager = FindObjectOfType<ScoringManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(ShouldSpawnNewBlock())
        {
            nextSpawn = Time.time + minimumTimeBetweenBlockSpawns;
            SpawnBlock();
        }
        
        bool ShouldSpawnNewBlock()
        {
            return Time.time > nextSpawn && activeBlock == null;
        }
        
        void SpawnBlock()
        {
            activeBlock = Instantiate(blockPrefabs[random.Next(0, blockPrefabs.Length)], blockSpawnPosition.transform.position, new Quaternion());
            activeBlock.AddComponent<RemoveFallenBlocks>();
            activeBlock.AddComponent<Block>();
            activeBlockScript = activeBlock.GetComponent<Block>();
            activeBlockScript.SetMovementDivider(blockMovementDivider);
        
            EventManager.TriggerEvent("BlockSpawned", activeBlock);
        }

    }

    private void FixedUpdate()
    {
        if(activeBlock == null)
                return;

        activeBlockScript.MoveDown(blockFallSpeed);
        
        EventManager.TriggerEvent("HeightChanged", HighestBlock());
    }
    
    private readonly List<GameObject> livingBlocks = new List<GameObject>();
    
    private Vector3 HighestBlock()
    {
        var heighest = Vector3.zero;
        foreach (var block in livingBlocks.Take(livingBlocks.Count - 1))
        {
            if (heighest.y < block.transform.position.y)
                heighest = block.transform.position;
        }

        return heighest;
    }

    private UnityAction<GameObject> onBlockSpawned;
    private UnityAction<GameObject> onBlockDisposed;
    private UnityAction onActiveBlockStoppedFalling;
    private UnityAction onRestartGame;
    private UnityAction onOpenMenu;
    public UnityAction<Vector3> onHeightChanged;

    private void Awake()
    {
        onActiveBlockStoppedFalling = ActiveBlockStoppedFalling;
        onRestartGame = () => SceneManager.LoadScene("Game");
        onOpenMenu = () => SceneManager.LoadScene("Menu");
        onBlockSpawned += (go) => scoringManager.OnBlockSpawned();
        onBlockSpawned += (go) => livingBlocks.Add(go);
        onBlockDisposed += (go) => scoringManager.OnBlockDisposed();
        onBlockDisposed += (go) => livingBlocks.Remove(go);
        onHeightChanged += (position) => blockSpawnPosition.transform.position = new Vector3(0, position.y + spawnHightBuffer, 0 ) ;
    }

    private void OnEnable()
    {
        EventManager.StartListening("ActiveBlockStoppedFalling", onActiveBlockStoppedFalling);
        EventManager.StartListening("BlockDisposed", onBlockDisposed);
        EventManager.StartListening("BlockSpawned", onBlockSpawned);
        EventManager.StartListening("HeightChanged", onHeightChanged);
        EventManager.StartListening("RestartGame", onRestartGame);
        EventManager.StartListening("OpenMenu", onOpenMenu);
    }

    private void OnDisable()
    {
        EventManager.StopListening("ActiveBlockStoppedFalling", onActiveBlockStoppedFalling);
        EventManager.StopListening("BlockDisposed", onBlockDisposed);
        EventManager.StopListening("BlockSpawned", onBlockSpawned);
        EventManager.StopListening("HeightChanged", onHeightChanged);
        EventManager.StopListening("RestartGame", onRestartGame);
        EventManager.StopListening("OpenMenu", onOpenMenu);
    }
    
    private void ActiveBlockStoppedFalling()
    {
        activeBlock = null;
        activeBlockScript = null;
    }
}
