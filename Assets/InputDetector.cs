using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputDetector : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            EventManager.TriggerEvent("MoveLeft");
        }
        
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            EventManager.TriggerEvent("MoveRight");
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            EventManager.TriggerEvent("Rotate");
        }
        
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            EventManager.TriggerEvent("DropDown");
        }
        
        if (Input.GetKeyDown(KeyCode.M))
        {
            EventManager.TriggerEvent("ToggleMusic");
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            EventManager.TriggerEvent("RestartGame");
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            EventManager.TriggerEvent("OpenMenu");
        }
    }
}
